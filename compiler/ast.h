/*
 * Copyright (C) 2017-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef AST_H
#define AST_H 1

#include "ast_node_type.h"
#include "utils.h"

struct bm_ast_node;

typedef int (*bm_ast_node_dtor)(struct bm_ast_node *node);
typedef int (*bm_ast_node_debug_data)(const struct bm_ast_node *node, int indent_lvl);

enum bm_ast_annotation_type
{
	BM_AST_ANNOTATION_MAIN = 0,
	BM_AST_ANNOTATION_READONLY,
	BM_AST_ANNOTATION_COUNT
};

enum bm_ast_annotation_mask
{
	BM_AST_ANNOTATION_MAIN_BIT           = 1u << BM_AST_ANNOTATION_MAIN,
	BM_AST_ANNOTATION_READONLY_BIT       = 1u << BM_AST_ANNOTATION_READONLY
};

enum bm_ast_visibility
{
	BM_AST_VISIBILITY_INVAL = 0,
	BM_AST_VISIBILITY_PUBLIC,
	BM_AST_VISIBILITY_PRIVATE,
	BM_AST_VISIBILITY_PROTECTED
};

enum bm_ast_val_imm_type
{
	BM_AST_VALUE_IMM_BOOL = 0,
	BM_AST_VALUE_IMM_CHAR,
	BM_AST_VALUE_IMM_FLOAT,
	BM_AST_VALUE_IMM_INT,
	BM_AST_VALUE_IMM_STRING,
	BM_AST_VALUE_IMM_UINT
};

enum bm_ast_bool
{
	BM_AST_BOOL_FALSE = 0,
	BM_AST_BOOL_TRUE
};

struct bm_ast_class_path
{
	struct buffer seqs;
};

struct bm_ast_type
{
	struct bm_ast_class_path class_path;
	char *class_name;
	unsigned long nb_generic_params;
	struct bm_ast_type *generic_params;
};

struct bm_ast_decl
{
	struct bm_ast_type type;
	char *name;
};

struct bm_ast_decl_func
{
	struct bm_ast_decl base;
	struct buffer param_names;
};

/* Miscellaneous nodes */

struct bm_ast_node_data_alias
{
	struct bm_ast_type tgt;
	struct bm_ast_type src;
};

enum bm_ast_node_class_child
{
	BM_AST_NODE_CLASS_CHILD_PROPS = 0,
	BM_AST_NODE_CLASS_CHILD_METHODS
};

struct bm_ast_node_data_class
{
	enum bm_ast_annotation_mask annotations;
	enum bm_ast_visibility visibility;
	struct bm_ast_type type;
	/* props, methods */
};

enum bm_ast_node_func_child
{
	BM_AST_NODE_FUNC_CHILD_BLOCK = 0
};

struct bm_ast_node_data_func
{
	enum bm_ast_annotation_mask annotations;
	enum bm_ast_visibility visibility;
	struct bm_ast_decl_func decl;
	/* block */
};

enum bm_ast_node_interface_child
{
	BM_AST_NODE_INTERFACE_CHILD_PROTO0 = 0
};

struct bm_ast_node_data_interface
{
	enum bm_ast_annotation_mask annotations;
	enum bm_ast_visibility visibility;
	struct bm_ast_type type;
	/* proto0, ... */
};

struct bm_ast_node_data_pack
{
	struct bm_ast_class_path path;
};

struct bm_ast_node_data_prop
{
	enum bm_ast_visibility visibility;
	struct bm_ast_decl decl;
};

struct bm_ast_node_data_proto
{
	struct bm_ast_decl_func decl;
};

struct bm_ast_node_data_use
{
	struct bm_ast_class_path path;
	char *name;
	char *alias_name;
};

/* Value nodes */

struct bm_ast_node_data_val_imm
{
	enum bm_ast_val_imm_type type;
	union
	{
		char              as_imm_char;
		enum bm_ast_bool  as_imm_bool;
		float             as_imm_float;
		int               as_imm_int;
		char             *as_imm_string;
		unsigned int      as_imm_uint;
	} data;
};

struct bm_ast_node_data_val_var
{
	char *name;
};

struct bm_ast_node_data_val_var_ext
{
	struct bm_ast_class_path class_path;
	char *class_name;
	char *name;
};

enum bm_ast_node_val_var_sub_child
{
	BM_AST_NODE_VAL_VAR_SUB_CHILD_EXPR = 0
};

struct bm_ast_node_data_val_var_sub
{
	char *name;
	/* expr */
};

/* Operator nodes */

enum bm_ast_node_binop_child
{
	BM_AST_NODE_BINOP_CHILD_LEFT_EXPR = 0,
	BM_AST_NODE_BINOP_CHILD_RIGHT_EXPR
};

struct bm_ast_node_data_binop
{
	int unused;
	/* left expr, right expr */
};

enum bm_ast_node_op_assign_child
{
	BM_AST_NODE_OP_ASSIGN_CHILD_LEFT_EXPR = 0,
	BM_AST_NODE_OP_ASSIGN_CHILD_RIGHT_EXPR
};

struct bm_ast_node_data_op_assign
{
	int unused;
	/* left expr, right expr */
};

enum bm_ast_node_op_call_child
{
	BM_AST_NODE_OP_CALL_CHILD_EXPR = 0,
	BM_AST_NODE_OP_CALL_CHILD_PARAM0
};

struct bm_ast_node_data_op_call
{
	int unused;
	/* expr, [param0, ...] */
};

enum bm_ast_node_op_cast_child
{
	BM_AST_NODE_OP_CAST_CHILD_EXPR = 0
};

struct bm_ast_node_data_op_cast
{
	struct bm_ast_type type;
	/* expr */
};

enum bm_ast_node_op_new_child
{
	BM_AST_NODE_OP_NEW_CHILD_PARAM0 = 0
};

struct bm_ast_node_data_op_new
{
	struct bm_ast_type type;
	/* [param0, ...] */
};

enum bm_ast_node_op_subscript_child
{
	BM_AST_NODE_OP_SUBSCRIPT_CHILD_LEFT_EXPR = 0,
	BM_AST_NODE_OP_SUBSCRIPT_CHILD_SUBSCRIPT_EXPR
};

struct bm_ast_node_data_op_subscript
{
	int unused;
	/* left expr, subscript expr */
};

enum bm_ast_node_op_ternary_cond_child
{
	BM_AST_NODE_OP_TERNARY_COND_LEFT_EXPR = 0,
	BM_AST_NODE_OP_TERNARY_COND_RIGHT_EXPR,
	BM_AST_NODE_OP_TERNARY_COND_MIDDLE_EXPR
};

struct bm_ast_node_data_op_ternary_cond
{
	int unused;
	/* left expr, right expr, middle expr */
};

enum bm_ast_node_postop_child
{
	BM_AST_NODE_POSTOP_CHILD_EXPR = 0
};

struct bm_ast_node_data_postop
{
	int unused;
	/* expr */
};

enum bm_ast_node_preop_child
{
	BM_AST_NODE_PREOP_CHILD_EXPR = 0
};

struct bm_ast_node_data_preop
{
	int unused;
	/* expr */
};

/* Instruction nodes */

struct bm_ast_node_data_instr_break
{
	int unused;
};

struct bm_ast_node_data_instr_continue
{
	int unused;
};

enum bm_ast_node_instr_decl_assign_child
{
	BM_AST_NODE_INSTR_DECL_ASSIGN_CHILD_EXPR = 0
};

struct bm_ast_node_data_instr_decl
{
	struct bm_ast_decl base;
	/* expr if decl_assign */
};

struct bm_ast_node_data_instr_noop
{
	int unused;
};

enum bm_ast_node_instr_ret_child
{
	BM_AST_NODE_INSTR_RET_CHILD_EXPR = 0
};

struct bm_ast_node_data_instr_ret
{
	int unused;
	/* [expr] */
};

/* Instruction container nodes */

enum bm_ast_node_block_child
{
	BM_AST_NODE_BLOCK_CHILD_INSTR0 = 0
};

struct bm_ast_node_data_block
{
	int unused;
	/* instr0, ... */
};

enum bm_ast_node_dowhile_child
{
	BM_AST_NODE_DOWHILE_CHILD_BLOCK = 0,
	BM_AST_NODE_DOWHILE_CHILD_EXPR
};

struct bm_ast_node_data_dowhile
{
	int unused;
	/* block, expr */
};

enum bm_ast_node_ifelse_child
{
	BM_AST_NODE_IFELSE_CHILD_EXPR = 0,
	BM_AST_NODE_IFELSE_CHILD_BLOCK_IF,
	BM_AST_NODE_IFELSE_CHILD_BLOCK_ELSE
};

struct bm_ast_node_data_ifelse
{
	int unused;
	/* expr, if block, else block */
};

enum bm_ast_node_loop_child
{
	BM_AST_NODE_LOOP_CHILD_BLOCK = 0
};

struct bm_ast_node_data_loop
{
	int count;
	/* block */
};

enum bm_ast_node_while_child
{
	BM_AST_NODE_WHILE_CHILD_EXPR = 0,
	BM_AST_NODE_WHILE_CHILD_BLOCK
};

struct bm_ast_node_data_while
{
	int unused;
	/* expr, block */
};

enum bm_ast_node_subexpr_child
{
	BM_AST_NODE_SUBEXPR_CHILD_EXPR = 0
};

struct bm_ast_node_data_subexpr
{
	int unused;
	/* expr */
};

union bm_ast_node_data
{
	/* Miscellaneous nodes */
	struct bm_ast_node_data_alias as_alias;
	struct bm_ast_node_data_class as_class;
	struct bm_ast_node_data_func as_func;
	struct bm_ast_node_data_interface as_interface;
	struct bm_ast_node_data_pack as_pack;
	struct bm_ast_node_data_prop as_prop;
	struct bm_ast_node_data_proto as_proto;
	struct bm_ast_node_data_use as_use;

	/* Value nodes */
	struct bm_ast_node_data_val_imm as_val_imm;
	struct bm_ast_node_data_val_var as_val_var;
	struct bm_ast_node_data_val_var_ext as_val_var_ext;
	struct bm_ast_node_data_val_var_sub as_val_var_sub;

	/* Operator nodes */
	struct bm_ast_node_data_binop as_binop;
	struct bm_ast_node_data_op_call as_op_call;
	struct bm_ast_node_data_op_cast as_op_cast;
	struct bm_ast_node_data_op_new as_op_new;
	struct bm_ast_node_data_op_subscript as_op_subscript;
	struct bm_ast_node_data_op_ternary_cond as_op_ternary_cond;
	struct bm_ast_node_data_postop as_postop;
	struct bm_ast_node_data_preop as_preop;

	/* Instruction nodes */
	struct bm_ast_node_data_instr_break as_instr_break;
	struct bm_ast_node_data_instr_continue as_instr_continue;
	struct bm_ast_node_data_instr_decl as_instr_decl;
	struct bm_ast_node_data_instr_noop as_instr_noop;
	struct bm_ast_node_data_instr_ret as_instr_ret;

	/* Instruction container nodes */
	struct bm_ast_node_data_block as_block;
	struct bm_ast_node_data_dowhile as_dowhile;
	struct bm_ast_node_data_ifelse as_ifelse;
	struct bm_ast_node_data_loop as_loop;
	struct bm_ast_node_data_subexpr as_subexpr;
	struct bm_ast_node_data_while as_while;
};

struct bm_ast_node
{
	enum bm_ast_node_type type;
	union bm_ast_node_data data;
	unsigned long children_count;
	struct bm_ast_node *children;
	bm_ast_node_dtor dtor;
	bm_ast_node_debug_data debug_data;
};

struct bm_ast
{
	struct bm_ast_node aliases;
	struct bm_ast_node classes;
	struct bm_ast_node funcs;
	struct bm_ast_node interfaces;
	struct bm_ast_node package;
	struct bm_ast_node uses;
	unsigned int has_main : 1;
};

#endif /* AST_H */
