/*
 * Copyright (C) 2017-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef PARSER_PRIV_H
#define PARSER_PRIV_H 1

#include "ast.h"
#include "lexer.h"

struct parser_ctx
{
	struct bm_ast ast;
	struct lexer_output *in;
	enum bm_ast_annotation_mask cur_annotations;
	unsigned int last_rshift_was_greater : 1;
};


extern int parser_ignore_eols(struct parser_ctx *ctx, unsigned long *cur_tok_id);
extern int parser_ignore_eols_or_semicolons(struct parser_ctx *ctx, unsigned long *cur_tok_id);
extern int parse_class_name(const char *name);
extern int parse_interface_name(const char *name);
extern int parse_var_name(const char *name);
extern int destroy_class_path(struct bm_ast_class_path *class_path);
extern int debug_class_path(const struct bm_ast_class_path *class_path);
extern int parse_class_path(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_class_path *out);
extern int clone_class_path(struct bm_ast_class_path *class_path, struct bm_ast_class_path *out);
extern int compare_class_path(struct bm_ast_class_path *a, struct bm_ast_class_path *b);
extern int destroy_type(struct bm_ast_type *type);
extern int debug_type(const struct bm_ast_type *type);
extern int parse_type(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_type *out);
extern int clone_type(struct bm_ast_type *type, struct bm_ast_type *out);
extern int parser_node_init_unknown(struct bm_ast_node *node);
extern int destroy_decl(struct bm_ast_decl *decl);
extern int debug_decl(const struct bm_ast_decl *decl, int indent_lvl);
extern int parse_decl(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_decl *out);
extern int parse_block(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out);

#endif /* PARSER_PRIV_H */
