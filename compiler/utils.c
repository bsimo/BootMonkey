/*
 * Copyright (C) 2017-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

#define LINE_CHUNK_SIZE 128


int buffer_init(struct buffer *buf)
{
	buf->nb_lines = 0;
	buf->nb_lines_allocated = 0;
	buf->line_chunk_size = LINE_CHUNK_SIZE;
	buf->lines = NULL;
	buf->total_size = 0;

	return 0;
}

int buffer_init_hint(struct buffer *buf, size_t line_chunk_size)
{
	if (!line_chunk_size)
		return -1;

	buf->nb_lines = 0;
	buf->nb_lines_allocated = 0;
	buf->line_chunk_size = line_chunk_size;
	buf->lines = NULL;
	buf->total_size = 0;

	return 0;
}

int buffer_clone(const struct buffer *buf, struct buffer *out)
{
	out->nb_lines = buf->nb_lines;
	out->nb_lines_allocated = buf->nb_lines_allocated;
	out->line_chunk_size = buf->line_chunk_size;
	out->total_size = buf->total_size;
	out->lines = NULL;

	if (!out->nb_lines || !out->total_size)
		return 0;

	out->lines = malloc(out->nb_lines_allocated * sizeof(out->lines[0]));
	if (!out->lines)
		goto failed_alloc_lines;

	out->lines[0] = malloc(out->total_size);
	if (!out->lines[0])
		goto failed_alloc_data;

	memcpy(out->lines[0], buf->lines[0], out->total_size);

	for (size_t i = 1; i < out->nb_lines; i++)
	{
		ptrdiff_t delta = buf->lines[i] - buf->lines[0];

		out->lines[i] = &out->lines[0][delta];
	}

	return 0;

failed_alloc_data:
	free(out->lines);

failed_alloc_lines:
	return buffer_init(out);
}

size_t buffer_get_line_count(const struct buffer *buf)
{
	return buf->nb_lines;
}

const char *buffer_get_line(const struct buffer *buf, size_t line)
{
	if (line >= buf->nb_lines)
		return NULL;

	return buf->lines[line];
}

int buffer_update_line(struct buffer *buf, const char *data, size_t line)
{
	size_t len, new_len;

	if (line >= buf->nb_lines)
		return -1;

	len = strlen(buf->lines[line]);
	new_len = strlen(data);

	if (new_len > len)
		return -1;

	memcpy(buf->lines[line], data, new_len + 1);
	return 0;
}

int buffer_append_line(struct buffer *buf, const char *line)
{
	size_t len;
	char **lines;
	char *scratch;

	len = strlen(line);

	if (buf->nb_lines + 1 > buf->nb_lines_allocated)
	{
		lines = realloc(buf->lines, (buf->nb_lines_allocated + buf->line_chunk_size) * sizeof(buf->lines[0]));
		if (!lines)
			return -1;

		buf->nb_lines_allocated += buf->line_chunk_size;
		buf->lines = lines;
	}

	buf->lines[buf->nb_lines] = NULL;

	scratch = realloc(buf->lines[0], buf->total_size + len + 1);
	if (!scratch)
		return -1;

	if (scratch != buf->lines[0])
	{
		size_t i;

		i = 1;
		while (i < buf->nb_lines)
		{
			ptrdiff_t idx = buf->lines[i] - buf->lines[0];

			buf->lines[i] = &scratch[idx];
			i++;
		}
	}
	buf->lines[0] = scratch;

	buf->lines[buf->nb_lines] = &buf->lines[0][buf->total_size];
	strncpy(buf->lines[buf->nb_lines], line, len + 1);

	buf->nb_lines++;
	buf->total_size += len + 1;

	return 0;
}

int buffer_remove_line(struct buffer *buf, size_t line)
{
	if (line >= buf->nb_lines)
		return -1;

	if (buf->nb_lines == 1)
	{
		buffer_free(buf);
		return 0;
	}

	if (line == 0)
	{
		size_t len;

		len = strlen(buf->lines[line + 1]);
		memmove(buf->lines[line], buf->lines[line + 1], len + 1);
		line = 1;
	}

	memmove(&buf->lines[line], &buf->lines[line + 1], (buf->nb_lines - (line + 1)) * sizeof(buf->lines[0]));
	buf->nb_lines--;

	return 0;
}

void buffer_free(struct buffer *buf)
{
	if (buf->lines)
	{
		if (buf->lines[0])
			free(buf->lines[0]);

		free(buf->lines);
	}

	buffer_init(buf);
}
