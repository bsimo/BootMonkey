/*
 * Copyright (C) 2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef UTIL_HASH_H
#define UTIL_HASH_H 1

#include <stddef.h>
#include <stdint.h>

#define U_HASH_FNV_U32_PRIME UINT32_C(0x01000193)
#define U_HASH_FNV_U32_OFFSET_BASIS UINT32_C(0x811c9dc5)
#define U_HASH_FNV_U64_PRIME UINT64_C(0x00000100000001B3)
#define U_HASH_FNV_U64_OFFSET_BASIS UINT64_C(0xcbf29ce484222325)

static const uint8_t u_hash_pearson_array256_shuffle[256] = {
	0xa3, 0x3a, 0xc8, 0x8e, 0x40, 0xd5, 0xc4, 0xb2, 0x95, 0x7e, 0x8d, 0x52, 0x91, 0x4a, 0xda, 0x49,
	0x0a, 0xb9, 0xa1, 0x4d, 0x87, 0x20, 0x66, 0xf1, 0x74, 0xdb, 0x69, 0x01, 0x2d, 0x78, 0xd4, 0x23,
	0x33, 0xcc, 0x35, 0x82, 0xb5, 0x89, 0x1c, 0x4c, 0x08, 0x1d, 0x41, 0x9c, 0x61, 0x93, 0x44, 0x27,
	0x22, 0x07, 0xd1, 0xa2, 0x34, 0x05, 0x70, 0x16, 0x0e, 0xca, 0x9d, 0x94, 0x06, 0xa8, 0x7b, 0xb4,
	0x14, 0x9f, 0xf7, 0x53, 0x48, 0x37, 0xa7, 0xc2, 0x13, 0xad, 0x8c, 0xfa, 0x8a, 0x09, 0x6a, 0x2e,
	0xe6, 0xb6, 0xb0, 0xe4, 0xaa, 0xbc, 0x9a, 0xd0, 0xcf, 0xe1, 0x3c, 0xbe, 0x18, 0xe9, 0xc3, 0x2c,
	0x1b, 0x12, 0xe0, 0x55, 0x81, 0x8b, 0x51, 0xe5, 0x1f, 0x43, 0x4f, 0xff, 0xf4, 0x56, 0x67, 0x9e,
	0x03, 0xf5, 0xcd, 0xc5, 0x75, 0xf3, 0x5b, 0x6c, 0xa9, 0xc6, 0x31, 0x00, 0xd8, 0x32, 0x0b, 0xe8,
	0xee, 0x30, 0x15, 0x65, 0x7c, 0x86, 0xb7, 0xb8, 0xed, 0x83, 0xeb, 0x5a, 0xdf, 0x4e, 0xf0, 0x57,
	0x7d, 0xd7, 0xa4, 0xfd, 0x73, 0x3b, 0xbf, 0x45, 0x97, 0x7a, 0xd9, 0xfe, 0xe2, 0x46, 0x5f, 0x28,
	0x92, 0xf6, 0x38, 0x3d, 0xa5, 0x59, 0xfb, 0x3e, 0x7f, 0xfc, 0x24, 0x17, 0x10, 0xaf, 0xdc, 0xe3,
	0xba, 0xce, 0xf9, 0x2a, 0x98, 0x88, 0x6b, 0x6e, 0x62, 0xcb, 0x96, 0x54, 0xac, 0x0d, 0x9b, 0xb3,
	0x36, 0xbb, 0x90, 0xa6, 0xbd, 0xd3, 0xae, 0xe7, 0x02, 0x2b, 0x5d, 0x5c, 0xc1, 0xb1, 0x60, 0x3f,
	0xdd, 0x26, 0xd2, 0x84, 0x85, 0x50, 0xc0, 0x0f, 0x68, 0x04, 0x29, 0x1a, 0x64, 0xc7, 0xef, 0xc9,
	0x5e, 0x80, 0xf8, 0x77, 0x6f, 0x42, 0x99, 0x79, 0x39, 0xec, 0x76, 0x2f, 0x71, 0x21, 0x4b, 0x0c,
	0x25, 0x63, 0x72, 0x8f, 0x1e, 0xde, 0xa0, 0x58, 0x19, 0xea, 0x47, 0xab, 0xd6, 0xf2, 0x11, 0x6d
};


static inline uint32_t u_hash_fnv1a_u32(const void *data, size_t len) {
	const unsigned char *d = (const unsigned char *)data;
	uint32_t r = U_HASH_FNV_U32_OFFSET_BASIS;

	while (len--)
		r = (r ^ *d++) * U_HASH_FNV_U32_PRIME;

	return r;
}

static inline uint64_t u_hash_fnv1a_u64(const void *data, size_t len) {
	const unsigned char *d = (const unsigned char *)data;
	uint64_t r = U_HASH_FNV_U64_OFFSET_BASIS;

	while (len--)
		r = (r ^ *d++) * U_HASH_FNV_U64_PRIME;

	return r;
}

static inline uint32_t u_hash_jenkins1_u32(const void *data, size_t len) {
	const unsigned char *d = (const unsigned char *)data;
	uint32_t r = 0;

	while (len--)
	{
		r += *d++;
		r += r << 10;
		r ^= r >> 6;
	}

	r += r << 3;
	r ^= r >> 11;
	r += r << 15;
	return r;
}

static inline uint8_t u_hash_pearson_u8_array(const void *data, size_t len, const uint8_t array[256]) {
	const unsigned char *d = (const unsigned char *)data;
	uint8_t r = 0;

	while (len--)
		r = array[r ^ *d++];

	return r;
}

static inline uint8_t u_hash_pearson_u8(const void *data, size_t len) {
	return u_hash_pearson_u8_array(data, len, u_hash_pearson_array256_shuffle);
}

static inline uint8_t u_hash_pjw_u8(const void *data, size_t len) {
	const unsigned char *d = (const unsigned char *)data;
	uint8_t r = 0;
	uint8_t high;
	const size_t byte_cnt = sizeof(r);
	const uint8_t high_mask = ((UINT8_C(1) << byte_cnt) - 1) << (7 * byte_cnt);

	while (len--)
	{
		r = (r << byte_cnt) + *d++;

		high = r & high_mask;
		if (high)
		{
			r ^= high >> (6 * byte_cnt);
			r &= ~high;
		}
	}

	return r;
}

static inline uint16_t u_hash_pjw_u16(const void *data, size_t len) {
	const unsigned char *d = (const unsigned char *)data;
	uint16_t r = 0;
	uint16_t high;
	const size_t byte_cnt = sizeof(r);
	const uint16_t high_mask = ((UINT16_C(1) << byte_cnt) - 1) << (7 * byte_cnt);

	while (len--)
	{
		r = (r << byte_cnt) + *d++;

		high = r & high_mask;
		if (high)
		{
			r ^= high >> (6 * byte_cnt);
			r &= ~high;
		}
	}

	return r;
}

static inline uint32_t u_hash_pjw_u32(const void *data, size_t len) {
	const unsigned char *d = (const unsigned char *)data;
	uint32_t r = 0;
	uint32_t high;
	const size_t byte_cnt = sizeof(r);
	const uint32_t high_mask = ((UINT32_C(1) << byte_cnt) - 1) << (7 * byte_cnt);

	while (len--)
	{
		r = (r << byte_cnt) + *d++;

		high = r & high_mask;
		if (high)
		{
			r ^= high >> (6 * byte_cnt);
			r &= ~high;
		}
	}

	return r;
}

static inline uint64_t u_hash_pjw_u64(const void *data, size_t len) {
	const unsigned char *d = (const unsigned char *)data;
	uint64_t r = 0;
	uint64_t high;
	const size_t byte_cnt = sizeof(r);
	const uint64_t high_mask = ((UINT64_C(1) << byte_cnt) - 1) << (7 * byte_cnt);

	while (len--)
	{
		r = (r << byte_cnt) + *d++;

		high = r & high_mask;
		if (high)
		{
			r ^= high >> (6 * byte_cnt);
			r &= ~high;
		}
	}

	return r;
}

#endif /* UTIL_HASH_H */
