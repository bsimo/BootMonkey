/*
 * Copyright (C) 2017-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>

#include "util/macro.h"
#include "util/misc.h"
#include "ast.h"
#include "debug.h"
#include "parser.h"

#include "parser_priv.h"
#include "parse_expr.h"
#include "parse_instr.h"

typedef int (*parse_expr_callback)(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out);
typedef int (*parse_expr_post_callback)(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *left_expr, struct bm_ast_node *out);

static int parse_expr_real(struct parser_ctx *ctx, unsigned long *cur_tok_id, int force_recurse, struct bm_ast_node *out);


static int val_imm_dtor(struct bm_ast_node *node)
{
	if (node->data.as_val_imm.type == BM_AST_VALUE_IMM_STRING
			&& node->data.as_val_imm.data.as_imm_string)
	{
		free(node->data.as_val_imm.data.as_imm_string);
		node->data.as_val_imm.data.as_imm_string = NULL;
	}

	return 0;
}

static int val_imm_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_val_imm *data = &node->data.as_val_imm;

	printf(",\n%*s\"value_type\": \"%s\"", indent_lvl * 2, "", debug_val_imm_type(data->type));
	printf(",\n%*s\"value\": ", indent_lvl * 2, "");
	switch (data->type)
	{
		case BM_AST_VALUE_IMM_BOOL:
			printf("%s", debug_bool(data->data.as_imm_bool));
			break;

		case BM_AST_VALUE_IMM_CHAR:
			printf("\"%c\"", data->data.as_imm_char);
			break;

		case BM_AST_VALUE_IMM_FLOAT:
			printf("%f", data->data.as_imm_float);
			break;

		case BM_AST_VALUE_IMM_INT:
			printf("%d", data->data.as_imm_int);
			break;

		case BM_AST_VALUE_IMM_STRING:
			printf("\"%s\"", data->data.as_imm_string);
			break;

		case BM_AST_VALUE_IMM_UINT:
			printf("%u", data->data.as_imm_uint);
			break;
	}

	return 0;
}

static int parse_val_imm(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	unsigned long i = *cur_tok_id;
	struct bm_ast_node val;

	parser_node_init(&val, BM_AST_NODE_VAL_IMM);
	val.dtor = val_imm_dtor;
	val.debug_data = val_imm_debug_data;

	switch (ctx->in->tokens[i].type)
	{
		case BM_TOK_FALSE:
			val.data.as_val_imm.type = BM_AST_VALUE_IMM_BOOL;
			val.data.as_val_imm.data.as_imm_bool = BM_AST_BOOL_FALSE;
			break;

		case BM_TOK_IMM_CHAR:
			val.data.as_val_imm.type = BM_AST_VALUE_IMM_CHAR;
			val.data.as_val_imm.data.as_imm_char = ctx->in->tokens[i].data.as_char;
			break;

		case BM_TOK_IMM_FLOAT:
			val.data.as_val_imm.type = BM_AST_VALUE_IMM_FLOAT;
			val.data.as_val_imm.data.as_imm_float = ctx->in->tokens[i].data.as_float;
			break;

		case BM_TOK_IMM_INT:
			val.data.as_val_imm.type = BM_AST_VALUE_IMM_INT;
			val.data.as_val_imm.data.as_imm_int = ctx->in->tokens[i].data.as_int;
			break;

		case BM_TOK_IMM_STR:
			val.data.as_val_imm.type = BM_AST_VALUE_IMM_STRING;
			val.data.as_val_imm.data.as_imm_string = u_strdup(ctx->in->tokens[i].data.as_str);
			if (!val.data.as_val_imm.data.as_imm_string)
				return -1;
			break;

		case BM_TOK_TRUE:
			val.data.as_val_imm.type = BM_AST_VALUE_IMM_BOOL;
			val.data.as_val_imm.data.as_imm_bool = BM_AST_BOOL_TRUE;
			break;

		default:
			return -1;
	}
	i++;

	*out = val;

	*cur_tok_id = i;
	return 0;
}

static int val_var_dtor(struct bm_ast_node *node)
{
	if (node->data.as_val_var.name)
	{
		free(node->data.as_val_var.name);
		node->data.as_val_var.name = NULL;
	}

	return 0;
}

static int val_var_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_val_var *data = &node->data.as_val_var;

	printf(",\n%*s\"name\": \"%s\"", indent_lvl * 2, "", data->name);

	return 0;
}

static int parse_val_var(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node val;

	parser_node_init(&val, BM_AST_NODE_VAL_VAR);
	val.dtor = val_var_dtor;
	val.debug_data = val_var_debug_data;

	val.data.as_val_var.name = NULL;

	if (ctx->in->tokens[i].type == BM_TOK_IDENTIFIER)
	{
		result = parse_var_name(ctx->in->tokens[i].data.as_str);
		if (result < 0)
			return -1;

		val.data.as_val_var.name = u_strdup(ctx->in->tokens[i].data.as_str);
		i++;
	}
	else if (ctx->in->tokens[i].type == BM_TOK_THIS)
	{
		val.data.as_val_var.name = u_strdup("this");
		i++;
	}
	else
		goto failed;

	if (!val.data.as_val_var.name)
		goto failed;

	*out = val;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&val);
	return -1;
}

static int val_var_ext_dtor(struct bm_ast_node *node)
{
	destroy_class_path(&node->data.as_val_var_ext.class_path);

	if (node->data.as_val_var_ext.class_name)
	{
		free(node->data.as_val_var_ext.class_name);
		node->data.as_val_var_ext.class_name = NULL;
	}

	if (node->data.as_val_var_ext.name)
	{
		free(node->data.as_val_var_ext.name);
		node->data.as_val_var_ext.name = NULL;
	}

	return 0;
}

static int val_var_ext_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_val_var_ext *data = &node->data.as_val_var_ext;
	int result;

	printf(",\n%*s\"class\": \"", indent_lvl * 2, "");

	result = debug_class_path(&data->class_path);
	if (result == 0)
		printf(".");

	printf("%s\"", data->class_name);

	printf(",\n%*s\"name\": \"%s\"", indent_lvl * 2, "", data->name);

	return 0;
}

static int parse_val_var_ext(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node val;

	parser_node_init(&val, BM_AST_NODE_VAL_VAR_EXT);
	val.dtor = val_var_ext_dtor;
	val.debug_data = val_var_ext_debug_data;

	val.data.as_val_var_ext.class_name = NULL;
	val.data.as_val_var_ext.name = NULL;

	result = parse_class_path(ctx, &i, &val.data.as_val_var_ext.class_path);
	if (result < 0)
		goto failed;

	if (ctx->in->tokens[i].type != BM_TOK_DOT)
		goto failed;
	i++;

	if (ctx->in->tokens[i].type != BM_TOK_IDENTIFIER)
		goto failed;

	result = parse_class_name(ctx->in->tokens[i].data.as_str);
	if (result < 0)
		goto failed;

	val.data.as_val_var_ext.class_name = u_strdup(ctx->in->tokens[i].data.as_str);
	if (!val.data.as_val_var_ext.class_name)
		goto failed;
	i++;

	if (ctx->in->tokens[i].type != BM_TOK_DOT)
		goto failed;
	i++;

	if (ctx->in->tokens[i].type != BM_TOK_IDENTIFIER)
		goto failed;

	result = parse_var_name(ctx->in->tokens[i].data.as_str);
	if (result < 0)
		goto failed;

	val.data.as_val_var_ext.name = u_strdup(ctx->in->tokens[i].data.as_str);
	if (!val.data.as_val_var_ext.name)
		goto failed;
	i++;

	*out = val;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&val);
	return -1;
}

static int val_var_sub_dtor(struct bm_ast_node *node)
{
	if (node->data.as_val_var_sub.name)
	{
		free(node->data.as_val_var_sub.name);
		node->data.as_val_var_sub.name = NULL;
	}

	return 0;
}

static int val_var_sub_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_val_var_sub *data = &node->data.as_val_var_sub;

	printf(",\n%*s\"name\": \"%s\"", indent_lvl * 2, "", data->name);

	return 0;
}

static int parse_val_var_sub(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *left_expr, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node val;

	if (ctx->in->tokens[i].type != BM_TOK_DOT)
		return -1;
	i++;

	parser_node_init(&val, BM_AST_NODE_VAL_VAR_SUB);
	val.dtor = val_var_sub_dtor;
	val.debug_data = val_var_sub_debug_data;

	if (ctx->in->tokens[i].type == BM_TOK_IDENTIFIER)
	{
		result = parse_var_name(ctx->in->tokens[i].data.as_str);
		if (result < 0)
			goto failed;

		val.data.as_val_var_sub.name = u_strdup(ctx->in->tokens[i].data.as_str);
		if (!val.data.as_val_var_sub.name)
			goto failed;
		i++;
	}
	else if (ctx->in->tokens[i].type == BM_TOK_NEW)
	{
		val.data.as_val_var_sub.name = u_strdup("new");
		if (!val.data.as_val_var_sub.name)
			goto failed;
		i++;
	}
	else
	{
		goto failed;
	}

	parser_node_set_child(&val, BM_AST_NODE_VAL_VAR_SUB_CHILD_EXPR, left_expr);

	*out = val;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&val);
	return -1;
}

static int parse_binop(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *left_expr, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	enum bm_ast_node_type type;
	struct bm_ast_node binop;
	struct bm_ast_node right_expr;

	switch (ctx->in->tokens[i].type)
	{
		case BM_TOK_OP_ADD:         type = BM_AST_NODE_BINOP_ADD; break;
		case BM_TOK_OP_AND:         type = BM_AST_NODE_BINOP_AND; break;
		case BM_TOK_OP_BITWISE_AND: type = BM_AST_NODE_BINOP_BITWISE_AND; break;
		case BM_TOK_OP_BITWISE_OR:  type = BM_AST_NODE_BINOP_BITWISE_OR; break;
		case BM_TOK_OP_BITWISE_XOR: type = BM_AST_NODE_BINOP_BITWISE_XOR; break;
		case BM_TOK_OP_DIV:         type = BM_AST_NODE_BINOP_DIV; break;
		case BM_TOK_OP_EQUAL:       type = BM_AST_NODE_BINOP_EQUAL; break;
		case BM_TOK_OP_GEQ:         type = BM_AST_NODE_BINOP_GEQUAL; break;
		case BM_TOK_OP_GREATER:     type = BM_AST_NODE_BINOP_GREATER; break;
		case BM_TOK_OP_LEQ:         type = BM_AST_NODE_BINOP_LEQUAL; break;
		case BM_TOK_OP_LOWER:       type = BM_AST_NODE_BINOP_LOWER; break;
		case BM_TOK_OP_MOD:         type = BM_AST_NODE_BINOP_MOD; break;
		case BM_TOK_OP_MUL:         type = BM_AST_NODE_BINOP_MUL; break;
		case BM_TOK_OP_NEQ:         type = BM_AST_NODE_BINOP_NEQUAL; break;
		case BM_TOK_OP_OR:          type = BM_AST_NODE_BINOP_OR; break;
		case BM_TOK_OP_POW:         type = BM_AST_NODE_BINOP_POW; break;
		case BM_TOK_OP_SHIFT_LEFT:  type = BM_AST_NODE_BINOP_LSHIFT; break;
		case BM_TOK_OP_SHIFT_RIGHT: type = BM_AST_NODE_BINOP_RSHIFT; break;
		case BM_TOK_OP_SUB:         type = BM_AST_NODE_BINOP_SUB; break;
		case BM_TOK_OP_XOR:         type = BM_AST_NODE_BINOP_XOR; break;
		default:                    type = BM_AST_NODE_UNKNOWN; break;
	}

	if (type == BM_AST_NODE_UNKNOWN)
		return -1;
	i++;

	parser_node_init(&binop, type);

	result = parse_expr_real(ctx, &i, 1, &right_expr);
	if (result < 0)
		goto failed;

	parser_node_set_child(&binop, BM_AST_NODE_BINOP_CHILD_LEFT_EXPR, left_expr);
	parser_node_set_child(&binop, BM_AST_NODE_BINOP_CHILD_RIGHT_EXPR, &right_expr);

	*out = binop;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&binop);
	return -1;
}

static int parse_op_assign(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *left_expr, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	enum bm_ast_node_type type;
	struct bm_ast_node assign;
	struct bm_ast_node right_expr;

	switch (left_expr->type)
	{
		case BM_AST_NODE_VAL_VAR:
		case BM_AST_NODE_VAL_VAR_EXT:
		case BM_AST_NODE_VAL_VAR_SUB:
		case BM_AST_NODE_OP_SUBSCRIPT:
			break;

		default:
			return -1;
	}

	switch (ctx->in->tokens[i].type)
	{
		case BM_TOK_OP_ASSIGN:             type = BM_AST_NODE_OP_ASSIGN; break;
		case BM_TOK_OP_ASSIGN_ADD:         type = BM_AST_NODE_OP_ASSIGN_ADD; break;
		case BM_TOK_OP_ASSIGN_BITWISE_AND: type = BM_AST_NODE_OP_ASSIGN_BITWISE_AND; break;
		case BM_TOK_OP_ASSIGN_BITWISE_OR:  type = BM_AST_NODE_OP_ASSIGN_BITWISE_OR; break;
		case BM_TOK_OP_ASSIGN_BITWISE_XOR: type = BM_AST_NODE_OP_ASSIGN_BITWISE_XOR; break;
		case BM_TOK_OP_ASSIGN_DIV:         type = BM_AST_NODE_OP_ASSIGN_DIV; break;
		case BM_TOK_OP_ASSIGN_MOD:         type = BM_AST_NODE_OP_ASSIGN_MOD; break;
		case BM_TOK_OP_ASSIGN_MUL:         type = BM_AST_NODE_OP_ASSIGN_MUL; break;
		case BM_TOK_OP_ASSIGN_POW:         type = BM_AST_NODE_OP_ASSIGN_POW; break;
		case BM_TOK_OP_ASSIGN_SHIFT_LEFT:  type = BM_AST_NODE_OP_ASSIGN_LSHIFT; break;
		case BM_TOK_OP_ASSIGN_SHIFT_RIGHT: type = BM_AST_NODE_OP_ASSIGN_RSHIFT; break;
		case BM_TOK_OP_ASSIGN_SUB:         type = BM_AST_NODE_OP_ASSIGN_SUB; break;
		default:                           type = BM_AST_NODE_UNKNOWN; break;
	}

	if (type == BM_AST_NODE_UNKNOWN)
		return -1;
	i++;

	parser_node_init(&assign, type);

	result = parse_expr_real(ctx, &i, 1, &right_expr);
	if (result < 0)
		goto failed;

	parser_node_set_child(&assign, BM_AST_NODE_OP_ASSIGN_CHILD_LEFT_EXPR, left_expr);
	parser_node_set_child(&assign, BM_AST_NODE_OP_ASSIGN_CHILD_RIGHT_EXPR, &right_expr);

	*out = assign;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&assign);
	return -1;
}

static int parse_op_call(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *left_expr, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	unsigned long param_id;
	struct bm_ast_node call;
	struct bm_ast_node param;

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_OPEN)
		return -1;
	i++;

	parser_node_init(&call, BM_AST_NODE_OP_CALL);

	param_id = BM_AST_NODE_OP_CALL_CHILD_PARAM0;

	while (i < ctx->in->nb_tokens)
	{
		result = parse_expr(ctx, &i, &param);
		if (result < 0)
			break;

		parser_node_set_child(&call, param_id, &param);
		param_id++;

		if (ctx->in->tokens[i].type != BM_TOK_COMMA)
			break;
		i++;
	}

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_CLOSE)
		goto failed;
	i++;

	parser_node_set_child(&call, BM_AST_NODE_OP_CALL_CHILD_EXPR, left_expr);

	*out = call;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&call);
	return -1;
}

static int op_cast_dtor(struct bm_ast_node *node)
{
	destroy_type(&node->data.as_op_cast.type);
	return 0;
}

static int op_cast_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_op_cast *data = &node->data.as_op_cast;

	printf(",\n%*s\"cast_type\": \"", indent_lvl * 2, "");
	debug_type(&data->type);
	printf("\"");

	return 0;
}

static int parse_op_cast(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node cast;
	struct bm_ast_node expr;

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_OPEN)
		return -1;
	i++;

	parser_node_init(&cast, BM_AST_NODE_OP_CAST);
	cast.dtor = op_cast_dtor;
	cast.debug_data = op_cast_debug_data;

	result = parse_type(ctx, &i, &cast.data.as_op_cast.type);
	if (result < 0)
		goto failed;

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_CLOSE)
		goto failed;
	i++;

	result = parse_expr_real(ctx, &i, 1, &expr);
	if (result < 0)
		goto failed;

	parser_node_set_child(&cast, BM_AST_NODE_OP_CAST_CHILD_EXPR, &expr);

	*out = cast;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&cast);
	return -1;
}

static int op_new_dtor(struct bm_ast_node *node)
{
	destroy_type(&node->data.as_op_new.type);
	return 0;
}

static int op_new_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_op_new *data = &node->data.as_op_new;

	printf(",\n%*s\"class\": \"", indent_lvl * 2, "");
	debug_type(&data->type);
	printf("\"");

	return 0;
}

static int parse_op_new(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	unsigned long param_id;
	struct bm_ast_node nnew;
	struct bm_ast_node param;

	if (ctx->in->tokens[i].type != BM_TOK_NEW)
		return -1;
	i++;

	parser_node_init(&nnew, BM_AST_NODE_OP_NEW);
	nnew.dtor = op_new_dtor;
	nnew.debug_data = op_new_debug_data;

	result = parse_type(ctx, &i, &nnew.data.as_op_new.type);
	if (result < 0)
		goto failed;

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_OPEN)
		goto failed;
	i++;

	param_id = BM_AST_NODE_OP_NEW_CHILD_PARAM0;

	while (i < ctx->in->nb_tokens)
	{
		result = parse_expr(ctx, &i, &param);
		if (result < 0)
			break;

		parser_node_set_child(&nnew, param_id, &param);
		param_id++;

		if (ctx->in->tokens[i].type != BM_TOK_COMMA)
			break;
		i++;
	}

	if (ctx->in->tokens[i].type != BM_TOK_BRACKET_CLOSE)
		goto failed;
	i++;

	*out = nnew;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&nnew);
	return -1;
}

static int parse_op_subscript(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *left_expr, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node subscript;
	struct bm_ast_node subscript_expr;

	if (ctx->in->tokens[i].type != BM_TOK_SQUARE_BRACKET_OPEN)
		return -1;
	i++;

	result = parse_expr(ctx, &i, &subscript_expr);
	if (result < 0)
		return -1;

	parser_node_init(&subscript, BM_AST_NODE_OP_SUBSCRIPT);
	parser_node_set_child(&subscript, BM_AST_NODE_OP_SUBSCRIPT_CHILD_SUBSCRIPT_EXPR, &subscript_expr);

	if (ctx->in->tokens[i].type != BM_TOK_SQUARE_BRACKET_CLOSE)
		goto failed;
	i++;

	parser_node_set_child(&subscript, BM_AST_NODE_OP_SUBSCRIPT_CHILD_LEFT_EXPR, left_expr);

	*out = subscript;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&subscript);
	return -1;
}

static int parse_op_ternary_cond(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *left_expr, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node ternary_cond;
	struct bm_ast_node expr;

	if (ctx->in->tokens[i].type != BM_TOK_QUESTION_MARK)
		return -1;
	i++;

	parser_node_init(&ternary_cond, BM_AST_NODE_OP_TERNARY_COND);

	result = parse_expr(ctx, &i, &expr);
	if (result < 0)
		goto failed;

	parser_node_set_child(&ternary_cond, BM_AST_NODE_OP_TERNARY_COND_MIDDLE_EXPR, &expr);

	if (ctx->in->tokens[i].type != BM_TOK_COLON)
		goto failed;
	i++;

	result = parse_expr_real(ctx, &i, 1, &expr);
	if (result < 0)
		goto failed;

	parser_node_set_child(&ternary_cond, BM_AST_NODE_OP_TERNARY_COND_LEFT_EXPR, left_expr);
	parser_node_set_child(&ternary_cond, BM_AST_NODE_OP_TERNARY_COND_RIGHT_EXPR, &expr);

	*out = ternary_cond;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&ternary_cond);
	return -1;
}

static int parse_postop(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *left_expr, struct bm_ast_node *out)
{
	unsigned long i = *cur_tok_id;
	enum bm_ast_node_type type;
	struct bm_ast_node postop;

	switch (ctx->in->tokens[i].type)
	{
		case BM_TOK_OP_DEC: type = BM_AST_NODE_POSTOP_DEC; break;
		case BM_TOK_OP_INC: type = BM_AST_NODE_POSTOP_INC; break;
		default:            type = BM_AST_NODE_UNKNOWN; break;
	}

	if (type == BM_AST_NODE_UNKNOWN)
		return -1;
	i++;

	parser_node_init(&postop, type);

	parser_node_set_child(&postop, BM_AST_NODE_POSTOP_CHILD_EXPR, left_expr);

	*out = postop;

	*cur_tok_id = i;
	return 0;
}

static int parse_preop(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	enum bm_ast_node_type type;
	struct bm_ast_node preop;
	struct bm_ast_node expr;

	switch (ctx->in->tokens[i].type)
	{
		case BM_TOK_OP_BITWISE_NOT: type = BM_AST_NODE_PREOP_BITWISE_NOT; break;
		case BM_TOK_OP_DEC:         type = BM_AST_NODE_PREOP_DEC; break;
		case BM_TOK_OP_INC:         type = BM_AST_NODE_PREOP_INC; break;
		case BM_TOK_OP_SUB:         type = BM_AST_NODE_PREOP_NEG; break;
		case BM_TOK_OP_NOT:         type = BM_AST_NODE_PREOP_NOT; break;
		default:                    type = BM_AST_NODE_UNKNOWN; break;
	}

	if (type == BM_AST_NODE_UNKNOWN)
		return -1;
	i++;

	parser_node_init(&preop, type);

	result = parse_expr_real(ctx, &i, 1, &expr);
	if (result < 0)
		goto failed;

	parser_node_set_child(&preop, BM_AST_NODE_PREOP_CHILD_EXPR, &expr);

	*out = preop;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&preop);
	return -1;
}

static int parse_dowhile(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node dowhile;
	struct bm_ast_node block;
	struct bm_ast_node cond;

	if (ctx->in->tokens[i].type != BM_TOK_DO)
		return -1;
	i++;

	parser_node_init(&dowhile, BM_AST_NODE_DOWHILE);

	result = parse_block(ctx, &i, &block);
	if (result < 0)
		goto failed;

	parser_node_set_child(&dowhile, BM_AST_NODE_DOWHILE_CHILD_BLOCK, &block);

	if (ctx->in->tokens[i].type != BM_TOK_WHILE)
		goto failed;
	i++;

	result = parse_expr(ctx, &i, &cond);
	if (result < 0)
		goto failed;

	parser_node_set_child(&dowhile, BM_AST_NODE_DOWHILE_CHILD_EXPR, &cond);

	*out = dowhile;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&dowhile);
	return -1;
}

static int parse_ifelse(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	unsigned long i_backup;
	struct bm_ast_node ifelse;
	struct bm_ast_node expr;
	struct bm_ast_node block_if;
	struct bm_ast_node block_else;

	if (ctx->in->tokens[i].type != BM_TOK_IF)
		return -1;
	i++;

	parser_node_init(&ifelse, BM_AST_NODE_IFELSE);

	result = parse_expr(ctx, &i, &expr);
	if (result < 0)
		goto failed;

	parser_node_set_child(&ifelse, BM_AST_NODE_IFELSE_CHILD_EXPR, &expr);

	parser_ignore_eols(ctx, &i);

	result = parse_block(ctx, &i, &block_if);
	if (result < 0)
		goto failed;

	parser_node_set_child(&ifelse, BM_AST_NODE_IFELSE_CHILD_BLOCK_IF, &block_if);

	i_backup = i;

	parser_ignore_eols(ctx, &i);

	if (ctx->in->tokens[i].type == BM_TOK_ELSE)
	{
		i++;

		parser_ignore_eols(ctx, &i);

		result = parse_block(ctx, &i, &block_else);

		if (result < 0)
			result = parse_ifelse(ctx, &i, &block_else);

		if (result < 0)
			goto failed;
	}
	else
	{
		parser_node_init_instr_noop(&block_else);

		i = i_backup;
	}

	parser_node_set_child(&ifelse, BM_AST_NODE_IFELSE_CHILD_BLOCK_ELSE, &block_else);

	*out = ifelse;

	*cur_tok_id = i;
	return 0;

failed:
    parser_node_destroy(&ifelse);
    return -1;
}

static int loop_debug_data(const struct bm_ast_node *node, int indent_lvl)
{
	const struct bm_ast_node_data_loop *data = &node->data.as_loop;

	printf(",\n%*s\"count\": %d", indent_lvl * 2, "", data->count);

	return 0;
}

static int parse_loop(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node loop;
	struct bm_ast_node block;

	if (ctx->in->tokens[i].type != BM_TOK_LOOP)
		return -1;
	i++;

	parser_node_init(&loop, BM_AST_NODE_LOOP);
	loop.debug_data = loop_debug_data;
	loop.data.as_loop.count = -1;

	if (ctx->in->tokens[i].type == BM_TOK_IMM_INT)
	{
	    loop.data.as_loop.count = ctx->in->tokens[i].data.as_int;
	    i++;
	}

	parser_ignore_eols(ctx, &i);

	result = parse_block(ctx, &i, &block);
	if (result < 0)
		return -1;

	parser_node_set_child(&loop, BM_AST_NODE_LOOP_CHILD_BLOCK, &block);

	*out = loop;

	*cur_tok_id = i;
	return 0;
}

static int parse_while(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;
	unsigned long i = *cur_tok_id;
	struct bm_ast_node nwhile;
	struct bm_ast_node cond;
	struct bm_ast_node block;

	if (ctx->in->tokens[i].type != BM_TOK_WHILE)
		return -1;
	i++;

	parser_node_init(&nwhile, BM_AST_NODE_WHILE);

	result = parse_expr(ctx, &i, &cond);
	if (result < 0)
		goto failed;

	parser_node_set_child(&nwhile, BM_AST_NODE_WHILE_CHILD_EXPR, &cond);

	parser_ignore_eols(ctx, &i);

	result = parse_block(ctx, &i, &block);
	if (result < 0)
		goto failed;

	parser_node_set_child(&nwhile, BM_AST_NODE_WHILE_CHILD_BLOCK, &block);

	*out = nwhile;

	*cur_tok_id = i;
	return 0;

failed:
	parser_node_destroy(&nwhile);
	return -1;
}

static const parse_expr_callback expr_callback_table[] = {
	parse_val_imm,
	/* val_var_ext must be parsed before val_var. */
	parse_val_var_ext,
	parse_val_var,
	parse_op_new,
	parse_preop,
	parse_block,
	parse_dowhile,
	parse_ifelse,
	parse_loop,
	parse_while
};

static const parse_expr_post_callback expr_post_callback_table[] = {
	parse_val_var_sub,
	parse_op_call,
	parse_op_assign,
	parse_op_subscript,
	parse_op_ternary_cond,
	parse_postop,
	parse_binop
};

static int parse_expr_real(struct parser_ctx *ctx, unsigned long *cur_tok_id, int force_recurse, struct bm_ast_node *out)
{
	int result;
	unsigned long id = *cur_tok_id;
	struct bm_ast_node tmp_expr;
	struct bm_ast_node expr;
	int has_brackets = 0;

	/* Casts must be handled before because of the brackets. */
	result = parse_op_cast(ctx, cur_tok_id, out);
	if (result == 0)
		return 0;

	if (!force_recurse && ctx->in->tokens[id].type == BM_TOK_BRACKET_OPEN)
	{
		has_brackets = 1;
		force_recurse = 1;
		id++;
	}

	for (unsigned int i = 0; i < ARRAY_SIZE(expr_callback_table); i++)
	{
		parse_expr_callback callback = expr_callback_table[i];

		result = callback(ctx, &id, &expr);
		if (result == 0)
			break;
	}

	/* Check for inner brackets. */
	if (result < 0 && force_recurse)
		result = parse_expr_real(ctx, &id, 0, &expr);

	if (result < 0)
		return -1;

	do
	{
		result = -1;

		for (unsigned int i = 0; i < ARRAY_SIZE(expr_post_callback_table); i++)
		{
			parse_expr_post_callback callback = expr_post_callback_table[i];

			result = callback(ctx, &id, &expr, &tmp_expr);
			if (result == 0)
			{
				expr = tmp_expr;
				break;
			}
		}
	}
	while (result == 0);

	if (has_brackets)
	{
		if (ctx->in->tokens[id].type != BM_TOK_BRACKET_CLOSE)
			goto failed;
		id++;

		parser_node_init(&tmp_expr, BM_AST_NODE_SUBEXPR);
		parser_node_set_child(&tmp_expr, BM_AST_NODE_SUBEXPR_CHILD_EXPR, &expr);

		expr = tmp_expr;
	}

	*out = expr;

	*cur_tok_id = id;
	return 0;

failed:
	parser_node_destroy(&expr);
	return -1;
}

enum op_pos
{
	OP_POS_INVAL = 0,
	OP_POS_LEFT,
	OP_POS_MIDDLE,
	OP_POS_RIGHT
};

struct op_props
{
	enum op_pos pos;
	int precedence;
	unsigned int is_assoc_ltr : 1;
};

static const struct op_props *get_op_props(enum bm_ast_node_type type)
{
	/* All uninitialized (no op) entries will have pos == 0 == OP_POS_INVAL. */
	static const struct op_props ops_props[BM_AST_NODE_COUNT] = {
		/* Right operators. */
		[BM_AST_NODE_OP_CALL]      = { OP_POS_RIGHT, -1, 1 },
		[BM_AST_NODE_OP_SUBSCRIPT] = { OP_POS_RIGHT, -1, 1 },
		[BM_AST_NODE_POSTOP_DEC]   = { OP_POS_RIGHT, -1, 1 },
		[BM_AST_NODE_POSTOP_INC ]  = { OP_POS_RIGHT, -1, 1 },
		[BM_AST_NODE_VAL_VAR_SUB]  = { OP_POS_RIGHT, -1, 1 },

		/* Left operators. */
		[BM_AST_NODE_OP_CAST]           = { OP_POS_LEFT, -2, 0 },
		[BM_AST_NODE_PREOP_BITWISE_NOT] = { OP_POS_LEFT, -2, 0 },
		[BM_AST_NODE_PREOP_DEC]         = { OP_POS_LEFT, -2, 0 },
		[BM_AST_NODE_PREOP_INC]         = { OP_POS_LEFT, -2, 0 },
		[BM_AST_NODE_PREOP_NEG]         = { OP_POS_LEFT, -2, 0 },
		[BM_AST_NODE_PREOP_NOT]         = { OP_POS_LEFT, -2, 0 },

		/* Binary operators. */
		[BM_AST_NODE_BINOP_DIV]             = { OP_POS_MIDDLE, -3,  1 },
		[BM_AST_NODE_BINOP_MOD]             = { OP_POS_MIDDLE, -3,  1 },
		[BM_AST_NODE_BINOP_MUL]             = { OP_POS_MIDDLE, -3,  1 },
		[BM_AST_NODE_BINOP_ADD]             = { OP_POS_MIDDLE, -4,  1 },
		[BM_AST_NODE_BINOP_SUB]             = { OP_POS_MIDDLE, -4,  1 },
		[BM_AST_NODE_BINOP_LSHIFT]          = { OP_POS_MIDDLE, -5,  1 },
		[BM_AST_NODE_BINOP_RSHIFT]          = { OP_POS_MIDDLE, -5,  1 },
		[BM_AST_NODE_BINOP_GEQUAL]          = { OP_POS_MIDDLE, -6,  1 },
		[BM_AST_NODE_BINOP_GREATER]         = { OP_POS_MIDDLE, -6,  1 },
		[BM_AST_NODE_BINOP_LEQUAL]          = { OP_POS_MIDDLE, -6,  1 },
		[BM_AST_NODE_BINOP_LOWER]           = { OP_POS_MIDDLE, -6,  1 },
		[BM_AST_NODE_BINOP_EQUAL]           = { OP_POS_MIDDLE, -7,  1 },
		[BM_AST_NODE_BINOP_NEQUAL]          = { OP_POS_MIDDLE, -7,  1 },
		[BM_AST_NODE_BINOP_BITWISE_AND]     = { OP_POS_MIDDLE, -8,  1 },
		[BM_AST_NODE_BINOP_BITWISE_XOR]     = { OP_POS_MIDDLE, -9,  1 },
		[BM_AST_NODE_BINOP_BITWISE_OR]      = { OP_POS_MIDDLE, -10, 1 },
		[BM_AST_NODE_BINOP_AND]             = { OP_POS_MIDDLE, -11, 1 },
		[BM_AST_NODE_BINOP_XOR]             = { OP_POS_MIDDLE, -12, 1 },
		[BM_AST_NODE_BINOP_OR]              = { OP_POS_MIDDLE, -13, 1 },

		/* Ternary condition operator. */
		[BM_AST_NODE_OP_TERNARY_COND]       = { OP_POS_MIDDLE, -14, 0 },

		/* Assignement operators. */
		[BM_AST_NODE_OP_ASSIGN]             = { OP_POS_MIDDLE, -15, 0 },
		[BM_AST_NODE_OP_ASSIGN_ADD]         = { OP_POS_MIDDLE, -15, 0 },
		[BM_AST_NODE_OP_ASSIGN_BITWISE_AND] = { OP_POS_MIDDLE, -15, 0 },
		[BM_AST_NODE_OP_ASSIGN_BITWISE_OR]  = { OP_POS_MIDDLE, -15, 0 },
		[BM_AST_NODE_OP_ASSIGN_BITWISE_XOR] = { OP_POS_MIDDLE, -15, 0 },
		[BM_AST_NODE_OP_ASSIGN_DIV]         = { OP_POS_MIDDLE, -15, 0 },
		[BM_AST_NODE_OP_ASSIGN_LSHIFT]      = { OP_POS_MIDDLE, -15, 0 },
		[BM_AST_NODE_OP_ASSIGN_MOD]         = { OP_POS_MIDDLE, -15, 0 },
		[BM_AST_NODE_OP_ASSIGN_MUL]         = { OP_POS_MIDDLE, -15, 0 },
		[BM_AST_NODE_OP_ASSIGN_POW]         = { OP_POS_MIDDLE, -15, 0 },
		[BM_AST_NODE_OP_ASSIGN_RSHIFT]      = { OP_POS_MIDDLE, -15, 0 },
		[BM_AST_NODE_OP_ASSIGN_SUB]         = { OP_POS_MIDDLE, -15, 0 }
	};

	if (type >= BM_AST_NODE_COUNT)
		return NULL;

	return &ops_props[type];
}

static int fix_op_precedence(struct bm_ast_node *op)
{
	const struct op_props *op_props;
	struct bm_ast_node *sub_op[2];
	const struct op_props *sub_op_props[2];
	int fixed;

	op_props = get_op_props(op->type);
	if (!op_props || op_props->pos == OP_POS_INVAL)
		return 0;

	do
	{
		unsigned long children_count = parser_node_get_children_count(op);

		fixed = 0;

		for (unsigned long i = 0; i < children_count && i < ARRAY_SIZE(sub_op); i++)
		{
			sub_op[i] = parser_node_get_child(op, i);

			fix_op_precedence(sub_op[i]);

			sub_op_props[i] = get_op_props(sub_op[i]->type);
		}

		switch (op_props->pos)
		{
			case OP_POS_LEFT:
			{
				if (sub_op_props[0]->pos == OP_POS_LEFT
						|| sub_op_props[0]->pos == OP_POS_INVAL)
					break;

				if (sub_op_props[0]->precedence >= op_props->precedence)
					break;

				op = parser_node_exchange_with_child(op, 0, 0);

				fixed = 1;
				break;
			}

			case OP_POS_RIGHT:
			{
				int sub_op_child_id;

				if (sub_op_props[0]->pos == OP_POS_RIGHT
						|| sub_op_props[0]->pos == OP_POS_INVAL)
					break;

				if (sub_op_props[0]->precedence >= op_props->precedence)
					break;

				sub_op_child_id = 0;
				if (sub_op_props[0]->pos == OP_POS_MIDDLE)
					sub_op_child_id = 1;

				op = parser_node_exchange_with_child(op, 0, sub_op_child_id);

				fixed = 1;
				break;
			}

			case OP_POS_MIDDLE:
			{
				if ((sub_op_props[0]->pos == OP_POS_LEFT || sub_op_props[0]->pos == OP_POS_MIDDLE)
						&& (sub_op_props[0]->precedence < op_props->precedence
							|| (sub_op_props[0]->precedence == op_props->precedence && !op_props->is_assoc_ltr)))
				{
					int sub_op_child_id;

					sub_op_child_id = 0;
					if (sub_op_props[0]->pos == OP_POS_MIDDLE)
						sub_op_child_id = 1;

					op = parser_node_exchange_with_child(op, 0, sub_op_child_id);
					fixed = 1;
				}

				if ((sub_op_props[1]->pos == OP_POS_MIDDLE || sub_op_props[1]->pos == OP_POS_RIGHT)
						&& (sub_op_props[1]->precedence < op_props->precedence
							|| (sub_op_props[1]->precedence == op_props->precedence && op_props->is_assoc_ltr)))
				{
					op = parser_node_exchange_with_child(op, 1, 0);
					fixed = 1;
				}
				break;
			}

			default:
				break;
		}
	}
	while (fixed);

	return 0;
}

int parse_expr(struct parser_ctx *ctx, unsigned long *cur_tok_id, struct bm_ast_node *out)
{
	int result;

	result = parse_expr_real(ctx, cur_tok_id, 1, out);
	if (result < 0)
		return -1;

	fix_op_precedence(out);

	return 0;
}
