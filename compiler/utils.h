/*
 * Copyright (C) 2017-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef UTILS_H
#define UTILS_H 1

#include <stddef.h>

struct buffer
{
	size_t nb_lines;
	size_t nb_lines_allocated;
	size_t line_chunk_size;
	char **lines;
	size_t total_size;
};


extern int buffer_init(struct buffer *buf);
extern int buffer_init_hint(struct buffer *buf, size_t line_chunk_size);
extern int buffer_clone(const struct buffer *buf, struct buffer *out);
extern size_t buffer_get_line_count(const struct buffer *buf);
extern const char *buffer_get_line(const struct buffer *buf, size_t line);
extern int buffer_update_line(struct buffer *buf, const char *data, size_t line);
extern int buffer_append_line(struct buffer *buf, const char *line);
extern int buffer_remove_line(struct buffer *buf, size_t line);
extern void buffer_free(struct buffer *buf);

#endif /* UTILS_H */
