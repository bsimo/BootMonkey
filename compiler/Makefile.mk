SUBDIRS-y := tests
EXTDIRS-y :=

bmc := bmc$(EXE_EXT)

BINS-y := $(bmc)

objs := \
	compiler.c.o \
	debug.c.o \
	lexer.c.o \
	utils.c.o
#	parse_expr.c.o \
	parse_instr.c.o \
	parser.c.o

CFLAGS-y  += -I$(MKS_PROJDIR)/include -I$(MKS_PROJDIR)
LDFLAGS-y +=

OBJS-$(bmc)-y := $(objs)

LIBS-$(bmc)-y := ../util/libutil.a
