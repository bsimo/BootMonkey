/*
 * Copyright (C) 2018-2019 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef UTIL_UTF8_H
#define UTIL_UTF8_H 1

/* UTF-8 is a superset of ASCII */

#include <stddef.h>
#include <stdint.h>

#define U_UTF8_MAX_LEN_UNLIMITED (SIZE_MAX)
#define U_UTF8_INVAL_CP (0xFFFFFFFF)
#define U_UTF8_REPLACEMENT_CP (0xFFFD)

typedef uint32_t u_utf8_cp_t;

struct u_utf8_ctx
{
	const char *str;
	size_t size;
	size_t cur;
};


void u_utf8_init(struct u_utf8_ctx *ctx, const char *str, size_t size);
void u_utf8_reset(struct u_utf8_ctx *ctx);
void u_utf8_rewind(struct u_utf8_ctx *ctx);
u_utf8_cp_t u_utf8_next(struct u_utf8_ctx *ctx, u_utf8_cp_t *disp_cp);
u_utf8_cp_t u_utf8_cur(struct u_utf8_ctx *ctx, u_utf8_cp_t *disp_cp);
int u_utf8_check(struct u_utf8_ctx *ctx, size_t *offset);
size_t u_utf8_len(struct u_utf8_ctx *ctx);
int u_utf8_cmp(struct u_utf8_ctx *ctx1, struct u_utf8_ctx *ctx2);
int u_utf8_strcmp_cstr(struct u_utf8_ctx *ctx1, const char *s);

/* The u_utf8_fast_*() functions assume that the string is valid UTF-8. */
int u_utf8_fast_cp_length(const char *s);
size_t u_utf8_fast_length(const char *str, size_t size);

#endif /* UTIL_UTF8_H */
