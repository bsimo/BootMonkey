/*
 * Copyright (C) 2019 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stddef.h>
#include <string.h>

#include "array_buffer.h"

#define DEFAULT_ELEMS_PER_SLICE 16

void u_array_buffer_init(struct u_array_buffer *ab, size_t elem_size, size_t elems_per_slice)
{
	if (elems_per_slice == 0)
		elems_per_slice = DEFAULT_ELEMS_PER_SLICE;

	u_buffer_init(&ab->buf, elems_per_slice * elem_size);
	ab->elem_size = elem_size;
	ab->elem_count = 0;
}

void u_array_buffer_fini(struct u_array_buffer *ab)
{
	u_buffer_fini(&ab->buf);
}

int u_array_buffer_push(struct u_array_buffer *ab, const void *elem)
{
	int result;

	result = u_buffer_append(&ab->buf, ab->elem_size, elem);
	if (result < 0)
		return result;

	ab->elem_count++;
	return 0;
}

int u_array_buffer_pop(struct u_array_buffer *ab, void *elem)
{
	int result;

	if (elem)
	{
		size_t elem_idx = ab->elem_count - 1;

		result = u_buffer_read(&ab->buf, elem_idx * ab->elem_size, elem, ab->elem_size);
		if (result < 0)
			return result;
	}

	result = u_buffer_shrink(&ab->buf, ab->elem_size);
	if (result < 0)
		return result;

	ab->elem_count--;
	return 0;
}

void *u_array_buffer_get(struct u_array_buffer *ab, size_t idx)
{
	size_t plane_idx;
	size_t plane_off;
	char *plane;

	if (idx >= ab->elem_count)
		return NULL;

	idx *= ab->elem_size;
	plane_idx = idx / ab->buf.plane_size;
	plane_off = idx % ab->buf.plane_size;

	plane = ab->buf.planes[plane_idx];
	return &plane[plane_off];
}

int u_array_buffer_set(struct u_array_buffer *ab, size_t idx, const void *data)
{
	void *p;

	p = u_array_buffer_get(ab, idx);
	if (!p)
		return -1;

	memcpy(p, data, ab->elem_size);
	return 0;
}
