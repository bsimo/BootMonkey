/*
 * Copyright (C) 2019 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef UTIL_ARRAY_BUFFER_H
#define UTIL_ARRAY_BUFFER_H 1

#include <stddef.h>

#include "buffer.h"

struct u_array_buffer
{
	struct u_buffer buf;
	size_t elem_size;
	size_t elem_count;
};

extern void u_array_buffer_init(struct u_array_buffer *ab, size_t elem_size, size_t elems_per_slice);
extern void u_array_buffer_fini(struct u_array_buffer *ab);
extern int u_array_buffer_push(struct u_array_buffer *ab, const void *elem);
extern int u_array_buffer_pop(struct u_array_buffer *ab, void *elem);
extern void *u_array_buffer_get(struct u_array_buffer *ab, size_t idx);
extern int u_array_buffer_set(struct u_array_buffer *ab, size_t idx, const void *data);

#endif /* UTIL_ARRAY_BUFFER_H */
