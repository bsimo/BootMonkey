/*
 * Copyright (C) 2019-2020 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef UTIL_ASCII_H
#define UTIL_ASCII_H 1

/* ASCII is a 7-bit subset of UTF-8 */

static inline int u_ascii_valid(char c)
{
	return (unsigned char)c <= 0x7F;
}

static inline int u_ascii_islower(char c)
{
	return c >= 'a' && c <= 'z';
}

static inline int u_ascii_isupper(char c)
{
	return c >= 'A' && c <= 'Z';
}

static inline int u_ascii_isalpha(char c)
{
	return u_ascii_islower(c) || u_ascii_isupper(c);
}

static inline int u_ascii_isdigit(char c)
{
	return c >= '0' && c <= '9';
}

static inline int u_ascii_isalnum(char c)
{
	return u_ascii_isalpha(c) || u_ascii_isdigit(c);
}

static inline int u_ascii_isspace(char c)
{
    return c == '\t'
            || c == '\n'
            || c == '\v'
            || c == '\f'
            || c == '\r'
            || c == ' ';
}

#endif /* UTIL_ASCII_H */
