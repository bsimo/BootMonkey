/*
 * Copyright (C) 2018-2019 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <limits.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "utf8.h"

#define USE_COMPILER_BUILTINS 1

#define USE_GCC_BUILTINS (USE_COMPILER_BUILTINS && __GNUC__)
#define USE_CLANG_BUILTINS (USE_COMPILER_BUILTINS && __clang__)


/* The *_fast_*() functions assume that the string is valid UTF-8. */

static inline int u_utf8_internal_fast_cp_length(const char *s) {
	unsigned char c = (unsigned char)s[0];

	if (c < 0x80)
		return 1;

#if USE_GCC_BUILTINS || USE_CLANG_BUILTINS

	static const int utf8_char_bit = 8;
	static const int delta_uint_char = CHAR_BIT * sizeof(unsigned int) - utf8_char_bit;

	return __builtin_clz(~((unsigned int)c << delta_uint_char));

#else

	if ((c & 0xE0) == 0xC0)
		return 2;
	if ((c & 0xF0) == 0xE0)
		return 3;
	return 4;

#endif
}

static inline size_t u_utf8_internal_fast_length(const char *s, size_t size) {
	size_t len = 0;
	const char *endp = &s[size];

	while (s < endp)
	{
		if ((*s & 0xC0) != 0x80)
			len++;
		s++;
	}

	return len;
}

void u_utf8_init(struct u_utf8_ctx *ctx, const char *str, size_t size)
{
	ctx->str = str;
	ctx->size = size;
	ctx->cur = 0;
}

void u_utf8_reset(struct u_utf8_ctx *ctx)
{
	ctx->str = NULL;
	ctx->size = 0;
	ctx->cur = 0;
}

void u_utf8_rewind(struct u_utf8_ctx *ctx)
{
	ctx->cur = 0;
}

u_utf8_cp_t u_utf8_next(struct u_utf8_ctx *ctx, u_utf8_cp_t *disp_cp)
{
	static const u_utf8_cp_t min_cps[] = {
		0, 0x80, 0x800, 0x10000
	};
	const unsigned char *str = (const unsigned char *)ctx->str;
	size_t cur;
	u_utf8_cp_t min_cp;
	u_utf8_cp_t cp;
	unsigned int count;

	cur = ctx->cur;
	if (cur >= ctx->size)
	{
		if (disp_cp)
			*disp_cp = U_UTF8_REPLACEMENT_CP;

		return U_UTF8_INVAL_CP;
	}

	cp = str[cur];

	if ((cp & 0x80) == 0)
		count = 1;
	else if ((cp & 0xE0) == 0xC0)
		count = 2;
	else if ((cp & 0xF0) == 0xE0)
		count = 3;
	else if ((cp & 0xF8) == 0xF0)
		count = 4;
	else
		goto inval;

	if (cur + count > ctx->size)
		goto inval;

	cur++;

	min_cp = min_cps[count - 1];
	cp &= 0xFF >> count;

	while (--count)
	{
		unsigned char c;

		c = str[cur];
		cur++;

		if ((c & 0xC0) != 0x80)
			goto inval;

		cp <<= 6;
		cp |= c & 0x3F;
	}

	/* A codepoint must be represented using the smallest number of bytes */
	if (cp < min_cp)
		goto inval;

	/* Reserved for UTF-16 surrogates */
	if (cp >= 0xD800 && cp <= 0xDFFF)
		goto inval;

	/* Out of range codepoints */
	if (cp >= 0x110000)
		goto inval;

	if (disp_cp)
		*disp_cp = cp;

	ctx->cur = cur;

	return cp;

inval:
	if (disp_cp)
		*disp_cp = U_UTF8_REPLACEMENT_CP;

	cp = str[ctx->cur];
	ctx->cur++;

	return cp;
}

u_utf8_cp_t u_utf8_cur(struct u_utf8_ctx *ctx, u_utf8_cp_t *disp_cp)
{
	size_t cur_backup;
	u_utf8_cp_t cp;

	cur_backup = ctx->cur;
	cp = u_utf8_next(ctx, disp_cp);
	ctx->cur = cur_backup;

	return cp;
}

int u_utf8_check(struct u_utf8_ctx *ctx, size_t *offset)
{
	int result = 0;
	size_t cur_backup;
	size_t off;
	u_utf8_cp_t cp;
	u_utf8_cp_t disp_cp;

	cur_backup = ctx->cur;
	ctx->cur = 0;

	while (1)
	{
		off = ctx->cur;

		cp = u_utf8_next(ctx, &disp_cp);
		if (cp == U_UTF8_INVAL_CP)
			break;

		if (disp_cp == U_UTF8_REPLACEMENT_CP
				&& disp_cp != cp)
		{
			result = -1;
			if (offset)
				*offset = off;
			break;
		}
	}

	ctx->cur = cur_backup;

	return result;
}

size_t u_utf8_len(struct u_utf8_ctx *ctx)
{
	size_t len = 0;
	size_t cur_backup;
	u_utf8_cp_t cp;

	cur_backup = ctx->cur;
	ctx->cur = 0;

	do
	{
		cp = u_utf8_next(ctx, NULL);
		len++;
	}
	while (cp != U_UTF8_INVAL_CP);

	len--;
	ctx->cur = cur_backup;

	return len;
}

int u_utf8_cmp(struct u_utf8_ctx *ctx1, struct u_utf8_ctx *ctx2)
{
	/* This works only if both are valid UTF-8 */
	if (ctx1->size != ctx2->size)
		return ctx1->size - ctx2->size;

	return memcmp(ctx1->str, ctx2->str, ctx1->size);
}

int u_utf8_strcmp_cstr(struct u_utf8_ctx *ctx1, const char *s)
{
	return strncmp(ctx1->str, s, ctx1->size);
}

int u_utf8_fast_cp_length(const char *str)
{
	return u_utf8_internal_fast_cp_length(str);
}

size_t u_utf8_fast_length(const char *str, size_t size)
{
	return u_utf8_internal_fast_length(str, size);
}
