SUBDIRS-y :=
EXTDIRS-y :=

libutil := libutil$(STATIC_LIB_EXT)

STATICLIBS-y := $(libutil)

objs := \
	array_buffer.c.o \
	buffer.c.o \
	file.c.o \
	misc.c.o \
	utf8.c.o

CFLAGS-y  += -I$(MKS_PROJDIR)/include -I$(MKS_PROJDIR)
LDFLAGS-y +=

OBJS-$(libutil)-y := $(objs)
