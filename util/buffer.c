/*
 * Copyright (C) 2019 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdlib.h>
#include <string.h>

#include "buffer.h"

void u_buffer_init(struct u_buffer *buffer, size_t plane_size)
{
	buffer->size = 0;
	buffer->plane_size = plane_size;
	buffer->plane_count = 0;
	buffer->planes = NULL;
}

void u_buffer_fini(struct u_buffer *buffer)
{
	while (buffer->plane_count)
		free(buffer->planes[--buffer->plane_count]);

	if (buffer->planes)
		free(buffer->planes);
}

int u_buffer_resize(struct u_buffer *buffer, size_t new_size)
{
	if (new_size == buffer->size)
		return 0;

	if (new_size < buffer->size)
		return u_buffer_shrink(buffer, buffer->size - new_size);

	return u_buffer_grow(buffer, new_size - buffer->size);
}

static inline size_t plane_count_from_buffer_size(size_t buffer_size, size_t plane_size) {
	return 1 + (buffer_size - 1) / plane_size;
}

int u_buffer_grow(struct u_buffer *buffer, size_t incr_size)
{
	size_t incr_plane_count;
	void **planes;

	if (!incr_size)
		return 0;

	incr_plane_count = plane_count_from_buffer_size(buffer->size + incr_size, buffer->plane_size) - buffer->plane_count;
	if (!incr_plane_count)
	{
		buffer->size += incr_size;
		return 0;
	}

	planes = realloc(buffer->planes, (buffer->plane_count + incr_plane_count) * sizeof(buffer->planes[0]));
	if (!planes)
		return -1;
	buffer->planes = planes;

	while (incr_plane_count)
	{
		buffer->planes[buffer->plane_count] = calloc(1, buffer->plane_size);
		if (!buffer->planes[buffer->plane_count])
			return -1;

		buffer->plane_count++;
		incr_plane_count--;
	}

	buffer->size += incr_size;
	return 0;
}

int u_buffer_shrink(struct u_buffer *buffer, size_t decr_size)
{
	size_t decr_plane_count;
	void **planes;

	if (!decr_size)
		return 0;

	if (decr_size > buffer->size)
		return -1;

	decr_plane_count = buffer->plane_count - plane_count_from_buffer_size(buffer->size - decr_size, buffer->plane_size);
	if (!decr_plane_count)
	{
		buffer->size -= decr_size;
		return 0;
	}

	while (decr_plane_count)
	{
		buffer->plane_count--;
		decr_plane_count--;

		free(buffer->planes[buffer->plane_count]);
	}

	if (buffer->plane_count > 0)
	{
		planes = realloc(buffer->planes, buffer->plane_count * sizeof(buffer->planes[0]));
		if (planes)
			buffer->planes = planes;
	}
	else
	{
		free(buffer->planes);
		buffer->planes = NULL;
	}

	buffer->size -= decr_size;
	return 0;
}

int u_buffer_append(struct u_buffer *buffer, size_t data_size, const void *data)
{
	int result;
	size_t old_size;

	old_size = buffer->size;

	result = u_buffer_grow(buffer, data_size);
	if (result < 0)
		return result;

	return u_buffer_write(buffer, old_size, data, data_size);
}

int u_buffer_char_at(struct u_buffer *buffer, size_t idx, char *c)
{
	size_t plane_idx;
	size_t plane_off;
	const char *plane;

	if (idx >= buffer->size)
		return -1;

	if (!c)
		return 0;

	plane_idx = idx / buffer->plane_size;
	plane_off = idx % buffer->plane_size;

	plane = (const char *)buffer->planes[plane_idx];

	*c = plane[plane_off];
	return 0;
}

static inline int minsz(size_t a, size_t b) {
	return a < b ? a : b;
}

static inline int maxsz(size_t a, size_t b) {
	return a > b ? a : b;
}

typedef int (*buffer_foreach_helper_callback_t)(void *plane_data, void *data, size_t size);

static int buffer_foreach_helper(struct u_buffer *buffer, buffer_foreach_helper_callback_t callback, size_t idx, void *data, size_t size)
{
	int result;
	char *d;
	size_t plane_idx;
	size_t plane_off;

	if (idx + size > buffer->size)
		return -1;

	if (!data || !size)
		return 0;

	d = (char *)data;
	plane_idx = idx / buffer->plane_size;
	plane_off = idx % buffer->plane_size;

	if (plane_off > 0)
	{
		size_t sz = minsz(size, buffer->plane_size - plane_off);
		char *plane = (char *)buffer->planes[plane_idx];

		result = callback(&plane[plane_off], d, sz);
		if (result != 0)
			return result;

		size -= sz;
		d += sz;
		plane_idx++;
	}

	while (size > 0)
	{
		size_t sz = minsz(size, buffer->plane_size);
		char *plane = (char *)buffer->planes[plane_idx];

		result = callback(plane, d, sz);
		if (result != 0)
			return result;

		size -= sz;
		d += sz;
		plane_idx++;
	}

	return 0;
}

static int buffer_read_callback(void *plane_data, void *data, size_t size)
{
	memcpy(data, plane_data, size);
	return 0;
}

int u_buffer_read(struct u_buffer *buffer, size_t idx, void *dst, size_t size)
{
	return buffer_foreach_helper(buffer, buffer_read_callback, idx, dst, size);
}

static int buffer_write_callback(void *plane_data, void *data, size_t size)
{
	memcpy(plane_data, data, size);
	return 0;
}

int u_buffer_write(struct u_buffer *buffer, size_t idx, const void *src, size_t size)
{
	return buffer_foreach_helper(buffer, buffer_write_callback, idx, (void *)src, size);
}

static int buffer_memcmp_callback(void *plane_data, void *data, size_t size)
{
	return memcmp(plane_data, data, size);
}

extern int u_buffer_memcmp(struct u_buffer *buffer, size_t idx, const void *data, size_t size)
{
	return buffer_foreach_helper(buffer, buffer_memcmp_callback, idx, (void *)data, size);
}
