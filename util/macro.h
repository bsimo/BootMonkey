/*
 * Copyright (C) 2015-2019 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef UTIL_MACRO_H
#define UTIL_MACRO_H 1

#include <stddef.h> /* offsetof() */
#include <stdint.h>

#define U_MARK_USED(var) ((void)(var))

#define U_CONTAINER_OF(ptr, type, member) ((type *)((uintptr_t)(ptr) - offsetof(type, member)))

#define U_ARGS_ISOLATE(...) __VA_ARGS__

#define U_STR(x) #x
#define U_STRLEN(x) (sizeof(x) - 1)
#define U_ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#endif /* UTIL_MACRO_H */
