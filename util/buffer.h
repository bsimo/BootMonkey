/*
 * Copyright (C) 2019 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef UTIL_BUFFER_H
#define UTIL_BUFFER_H 1

#include <stddef.h>

struct u_buffer
{
	size_t size;
	size_t plane_size;
	size_t plane_count;
	void **planes;
};

extern void u_buffer_init(struct u_buffer *buffer, size_t plane_size);
extern void u_buffer_fini(struct u_buffer *buffer);
extern int u_buffer_grow(struct u_buffer *buffer, size_t incr_size);
extern int u_buffer_shrink(struct u_buffer *buffer, size_t decr_size);
extern int u_buffer_resize(struct u_buffer *buffer, size_t new_size);
extern int u_buffer_append(struct u_buffer *buffer, size_t data_size, const void *data);
extern int u_buffer_char_at(struct u_buffer *buffer, size_t idx, char *c);
extern int u_buffer_read(struct u_buffer *buffer, size_t idx, void *dst, size_t size);
extern int u_buffer_write(struct u_buffer *buffer, size_t idx, const void *src, size_t size);
extern int u_buffer_memcmp(struct u_buffer *buffer, size_t idx, const void *data, size_t size);

#endif /* UTIL_BUFFER_H */
